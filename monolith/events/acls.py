from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):

    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization":PEXELS_API_KEY}
    params = {"query": f"{city} {state}",
              "per_page":1,
               }

    response = requests.get(url, headers=headers, params=params)

    photo = response.json()

    photo_link = photo["photos"][0]["src"]["original"]

    picture_url = {
       "picture_url":photo_link
    }

    return picture_url

def get_weather(city, state):

    us_country_code = 840

    url = "https://api.openweathermap.org/data/2.5/weather"
    # headers = {"appid": OPEN_WEATHER_API_KEY}

    params = {
        "q": [city, state, us_country_code],
        "appid": OPEN_WEATHER_API_KEY
    }
    # print(params_name)
    response_geo_coord = requests.get(url, params=params)

    geo_coord = response_geo_coord.json()
    # print(geo_coord)

    # The info below is print(geo_coord)
    #{'coord': {'lon': -121.895, 'lat': 37.3394}, 'weather': [{'id': 801, 'main': 'Clouds', 'description': 'few clouds', 'icon': '02n'}], 'base': 'stations', 'main': {'temp': 284.86, 'feels_like': 283.52, 'temp_min': 282.99, 'temp_max': 287.23, 'pressure': 1019, 'humidity': 55}, 'visibility': 10000, 'wind': {'speed': 4.02, 'deg': 2, 'gust': 4.02}, 'clouds': {'all': 20}, 'dt': 1701138699, 'sys': {'type': 2, 'id': 2004102, 'country': 'US', 'sunrise': 1701097157, 'sunset': 1701132688}, 'timezone': -28800, 'id': 5392171, 'name': 'San Jose', 'cod': 200}

    lat = geo_coord["coord"]["lat"]
    lon = geo_coord["coord"]["lon"]

    params = {
        "lat":lat,
        "lon":lon,
        "units":"imperial",
        "appid": OPEN_WEATHER_API_KEY
    }

    response_weather = requests.get(url, params=params)

    weather = response_weather.json()
    # print(weather)
    # weather = None #This was used to test null case

    if weather:
        weather_detail = {
            "temp": weather["main"]["temp"],
            "description": weather["weather"][0]["description"],
        }

    else:
        weather_detail = None

    # print(weather_detail)
    return weather_detail


#This is print(weather_detail)
# {"page":1,"per_page":1,
#  "photos":[{
#      "id":3410816,
#      "width":2982,
#      "height":3982,
#      "url":"https://www.pexels.com/photo/hollywood-sign-viewing-houses-under-blue-and-white-sky-3410816/",
#      "photographer":"Vinicius Maciel","photographer_url":"https://www.pexels.com/@vinicius-maciel-1820405",
#      "photographer_id":1820405,
#      "avg_color":"#817672",
#      "src":{"original":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg",
#      "large2x":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940","large":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940","medium":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350","small":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130","portrait":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800","landscape":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200","tiny":"https://images.pexels.com/photos/3410816/pexels-photo-3410816.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"},"liked":false,"alt":"Hollywood Sign Viewing Houses Under Blue and White Sky"}],"total_results":4524,"next_page":"https://api.pexels.com/v1/search/?page=2\u0026per_page=1\u0026query=Los+Angeles+CA"}
