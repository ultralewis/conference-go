from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]

@require_http_methods({"GET", "POST"})
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()

        return JsonResponse(
            {"conferences": conferences},
            ConferenceListEncoder,
            False,
            )
    else:
        content = json.loads(request.body)

        try:
            location = Location.objects.get(id = content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message:" : "Invalid location id"},
                status = 400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            ConferenceDetailEncoder,
            False
        )

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=id)

        weather = get_weather(conference.location.city, conference.location.state)

        return JsonResponse(
            {"conference": conference,
             "weather": weather},
            ConferenceDetailEncoder,
            False,
        )

    if request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse(
            {"delete:": count > 0}
        )
    else:
        #This is PUT. We have to worry about location which has a State (edge case)
        # To test, http://localhost:8000/api/conferences/2/ and used "{"location": 2}".
        # It changed location from "Georgia World Congress Center" to "Orange County Convention Center." Changed back to normal
        content = json.loads(request.body)

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            ConferenceDetailEncoder,
            False,
        )

@require_http_methods(["GET", "POST"])
def api_list_locations(request):

    if request.method == "GET":
        locations = Location.objects.all()

        return JsonResponse(
            {"locations":locations},
            LocationListEncoder,
            False)

    else: #POST

        #Will will have get_picture_url here. We will also include in JsonResponse i.e. "picture_url": URL of a picture of the city,

        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation = content["state"])
            content["state"] = state
        except:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400
            )

        photo_url = get_photo(content["city"], content["state"])
        content.update(photo_url)

        location = Location.objects.create(**content)

        return JsonResponse(
            location,
            LocationDetailEncoder,
            False
        )

    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

#!xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            LocationDetailEncoder,
            False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse(
            {"delete" : count > 0}
        )
    else: #This is "PUT" this updates the data
        content = json.loads(request.body) #content is a dictionary

        try:
            if "state" in content:
                state = State.objects.get(abbreviation = content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid state abbreviation"},
                status = 400,
            )
        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            LocationDetailEncoder,
            False
        )
